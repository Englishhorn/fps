# FPS (First Projects Supervision)
Started: 06.12.2021

Planned duration: ~ 3 weeks

## Functional requirements:
**The application should have following functionality:**
* Web pages with list of all projects, list of all people and info about each project
* Creation and modification of projects and people by supervisors
* Assigning people to projects with STUDENT or MENTOR role
* _Optional:_ page with info about each person and history of their participation


## Technical requirements:
Technologies: Java 11, Spring Boot, Hibernate, Maven, Junit 5, Bootstrap 4

* Follow all naming conventions and keep project well-structured
* Use Layered and MVC architecture patterns
* Try to use GoF patterns where applicable
* Develop Frontend UI
* Write unit tests
* Use Git as VCS
* Follow Agile principles
* Data should be stored in DB

## How to run locally
To run DB you need to:
* Remove '.example' ending from /src/main/resources/application.yml.example
* Add your login data into 'username' and 'password' fields 
