package com.epam.fps.service;

import static org.assertj.core.api.AssertionsForClassTypes.assertThat;
import static org.assertj.core.api.AssertionsForClassTypes.catchThrowable;

import com.epam.fps.entities.KeySkill;
import com.epam.fps.entities.User;
import com.epam.fps.exceptions.EntityNotFoundException;
import com.epam.fps.repository.UserRepository;
import java.util.HashSet;
import java.util.Optional;
import java.util.Set;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.Spy;
import org.mockito.junit.jupiter.MockitoExtension;

@ExtendWith(MockitoExtension.class)
public class UserServiceTest {
  @Mock
  UserRepository repository;

  @Mock
  KeySkillService keySkillService;

  @Spy
  @InjectMocks
  UserService userService;

  @Test
  void create_userFromUi_savedUser() {

    // GIVEN
    String expectedName = "John";
    Long id = 1L;
    User userFromUi = User.builder()
        .id(id)
        .name(expectedName)
        .email("john@mail.com")
        .build();

    Mockito.when(repository.existsById(id)).thenReturn(false);
    Mockito.when(repository.findById(id)).thenReturn(Optional.of(userFromUi));
    Mockito.doReturn(new HashSet<KeySkill>()).when(userService).compareSkillsWithDb(Mockito.any());

    // WHEN
    userService.create(userFromUi);

    // THEN
    assertThat(userService.readById(id).getName()).isEqualTo(expectedName);

    Mockito.verify(repository).existsById(id);
    Mockito.verify(repository).save(userFromUi);
    Mockito.verify(repository, Mockito.times(1)).findById(id);
  }

  @Test
  void readById_existingId_userById() {

    // GIVEN
    Long id = 1L;
    User userFromDb = User.builder()
        .id(id)
        .name("John")
        .email("john@mail.com")
        .build();

    Mockito.when(repository.findById(id)).thenReturn(Optional.of(userFromDb));

    // WHEN
    userService.readById(id);

    // THEN
    assertThat(userService.readById(id).getName()).isEqualTo(userFromDb.getName());
    assertThat(userService.readById(id).getEmail()).isEqualTo(userFromDb.getEmail());
    assertThat(userService.readById(id).getKeySkills()).isEqualTo(userFromDb.getKeySkills());

    Mockito.verify(repository, Mockito.times(4)).findById(id);
  }

  @Test
  void readById_notExistsId_exceptionThrown() {

    // GIVEN
    String exceptionMessage = "User with this id doesn't exists";
    Long id = 1L;

    Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

    // WHEN
    Throwable throwable = catchThrowable(() -> userService.readById(id));

    // THEN
    assertThat(throwable).isInstanceOf(EntityNotFoundException.class)
        .hasMessage(exceptionMessage);

    Mockito.verify(repository, Mockito.never()).save(Mockito.any());
  }

  @Test
  void update_existingUserWithEmail_userEmailUpdated() {

    // GIVEN
    String expectedEmail = "updated@mail.com";
    Long id = 1L;
    User userFromUi = User.builder()
        .email(expectedEmail)
        .build();
    User existingUser = User.builder()
        .id(id)
        .email("mail@mail.com")
        .build();

    Mockito.when(repository.findById(id)).thenReturn(Optional.of(existingUser));
    Mockito.doReturn(new HashSet<KeySkill>()).when(userService).compareSkillsWithDb(Mockito.any());

    // WHEN
    userService.update(userFromUi, id);

    // THEN
    assertThat(existingUser.getEmail()).isEqualTo(expectedEmail);

    Mockito.verify(repository, Mockito.times(1)).findById(id);
    Mockito.verify(repository).save(existingUser);
  }

  @Test
  void update_notExistsId_exceptionThrown() {

    // GIVEN
    String exceptionMessage = "User with this id doesn't exists";
    Long id = 1L;
    User userFromUi = User.builder()
        .build();

    Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

    // WHEN
    Throwable throwable = catchThrowable(() -> userService.update(userFromUi, id));

    // THEN
    assertThat(throwable).isInstanceOf(EntityNotFoundException.class)
        .hasMessage(exceptionMessage);

    Mockito.verify(repository, Mockito.never()).save(Mockito.any());
  }

  @Test
  void delete_existsUserIdToDelete_exceptionThrown() {

    // GIVEN
    String exceptionMessage = "User with this id doesn't exists";
    Long id = 1L;
    User existingUser = User.builder()
        .id(id)
        .build();

    repository.save(existingUser);
    Mockito.when(repository.existsById(id)).thenReturn(true);
    Mockito.when(repository.findById(id)).thenReturn(Optional.empty());

    // WHEN
    userService.delete(id);
    Throwable throwable = catchThrowable(() -> userService.readById(id));

    // THEN
    assertThat(throwable).isInstanceOf(EntityNotFoundException.class)
        .hasMessage(exceptionMessage);

    Mockito.verify(repository).save(existingUser);
    Mockito.verify(repository).existsById(id);
    Mockito.verify(repository).findById(id);
    Mockito.verify(repository).deleteById(id);
  }

  @Test
  void delete_notExistsUserIdToDelete_exceptionThrown() {

    // GIVEN
    String exceptionMessage = "User with this id doesn't exists";
    Long id = 1L;

    Mockito.when(repository.existsById(id)).thenReturn(false);

    // WHEN
    Throwable throwable = catchThrowable(() -> userService.delete(id));

    // THEN
    assertThat(throwable).isInstanceOf(EntityNotFoundException.class)
        .hasMessage(exceptionMessage);

    Mockito.verify(repository).existsById(id);
    Mockito.verify(repository, Mockito.never()).deleteById(id);
  }

  @Test
  void compareSkillsWithDb_userWithSkills_setOfSkills() {

    // GIVEN
    KeySkill java = KeySkill.builder()
        .name("Java")
        .build();
    KeySkill sql = KeySkill.builder()
        .name("SQL")
        .build();
    KeySkill oop = KeySkill.builder()
        .name("OOP")
        .build();
    Set<KeySkill> keySkills = Set.of(java, sql, oop);
    User user = User.builder()
        .keySkills(keySkills)
        .build();

    Mockito.when(keySkillService.getOrCreate("Java")).thenReturn(java);
    Mockito.when(keySkillService.getOrCreate("SQL")).thenReturn(sql);
    Mockito.when(keySkillService.getOrCreate("OOP")).thenReturn(oop);

    // WHEN
    Set<KeySkill> expectedKeySkills = userService.compareSkillsWithDb(user);

    // THEN
    assertThat(expectedKeySkills).isEqualTo(keySkills);

    Mockito.verify(keySkillService).getOrCreate("Java");
    Mockito.verify(keySkillService).getOrCreate("SQL");
    Mockito.verify(keySkillService).getOrCreate("OOP");
  }
}