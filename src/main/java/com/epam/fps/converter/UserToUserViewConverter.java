package com.epam.fps.converter;

import com.epam.fps.dto.UserView;
import com.epam.fps.entities.KeySkill;
import com.epam.fps.entities.Project;
import com.epam.fps.entities.ProjectRole;
import com.epam.fps.entities.User;
import java.util.List;
import java.util.Set;
import java.util.stream.Collectors;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class UserToUserViewConverter implements Converter<User, UserView> {

  private static final String COMMA_WITH_SPACE = ", ";

  @Override
  public UserView convert(User user) {

    Set<KeySkill> keySkills = user.getKeySkills();
    List<ProjectRole> projectRoles = user.getProjectRoles();

    return UserView.builder()
        .id(user.getId())
        .name(user.getName())
        .keySkills(castSkillsToString(keySkills))
        .projects(castRolesToString(projectRoles))
        .email(user.getEmail())
        .build();
  }

  String castRolesToString(List<ProjectRole> roles) {

    return roles.stream()
        .map(ProjectRole::getProject)
        .map(Project::getName)
        .collect(Collectors.joining(COMMA_WITH_SPACE));
  }

  String castSkillsToString(Set<KeySkill> keySkills) {

    return keySkills.stream()
        .map(KeySkill::getName)
        .collect(Collectors.joining(COMMA_WITH_SPACE));
  }
}
