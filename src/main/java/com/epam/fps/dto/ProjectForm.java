package com.epam.fps.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class ProjectForm {

  private Long id;
  private String name;
  private String startDate;
  private String technologies;
  private String users;
}
