package com.epam.fps.dto;

import javax.validation.constraints.Email;
import javax.validation.constraints.Size;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Getter
@Setter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class UserForm {

  private static final String EMAIL_PATTERN = "[a-zA-Z0-9._%+-]+@[a-z0-9.-]+\\.[a-z]{2,4}$";

  private Long id;

  @Size(min = 2, max = 20, message = "Name should be between 2 and 20 characters")
  private String name;

  @Email(regexp = EMAIL_PATTERN, message = "Email should be valid, like mail@mail.com")
  private String email;

  @Size(min = 1, message = "Key skills should be minimal length 1 characters")
  private String keySkills;
}
