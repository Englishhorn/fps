package com.epam.fps.dto;

import com.epam.fps.entities.ProjectRole;
import java.util.List;
import lombok.Builder;
import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
@Builder
public class ProjectView {

  private Long id;
  private String name;
  private String startDate;
  private String endDate;
  List<ProjectRole> roles;
  private String technologies;
}
